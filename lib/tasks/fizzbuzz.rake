namespace :fizzbuzz do
  desc "Automated shopping for testing pusher feed"
  task roboshop: :environment do

    robousers = User.limit(10).offset(2)
    
    now = Time.now
    counter = 1
    loop do
      if Time.now < now + counter
        next
      else
        
        #GENERATE AN ORDER
        puts "Generating a random order..."
        u = robousers[rand(1..19)]
        order = Order.new
        order_item = order.order_items.new({
          quantity: rand(1..5), 
          product_id:rand(1..99) 
        })
        order.save
        
        # TRIGGER PUSHER 
        Pusher['updates_channel'].trigger('app_update', { 
          order: order, 
          products: order.order_items, 
          user: u 
        })
        puts "Order saved, watch the stats page for the fizz...\n\n"
        
      end
      counter += 3
      break if counter > 120
    end

  end
end
