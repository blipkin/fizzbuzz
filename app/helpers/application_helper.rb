module ApplicationHelper

  # Handle titles
  def title 
    "F¡zzBuzz!"
  end
  
  def full_title(page_title = '')
    base_title = title
    if page_title.empty?
      base_title
    else
      page_title + " | " + base_title
    end
  end
end
