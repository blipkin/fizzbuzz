class OrderItemsController < ApplicationController
  
  # Add to cart
  def create
    @order = current_order
    @order_item = @order.order_items.new(order_item_params)
    @order.save
    session[:order_id] = @order.id
    item = Product.find(order_item_params[:product_id])
    q = order_item_params[:quantity]
    quantity = q != '1' ? "(#{q})" : ""
    flash[:success] = "#{item.title}#{quantity} successfully added to cart!"
    redirect_to store_url
  end

  def update
    @order = current_order
    @order_item = @order.order_items.find(params[:id])
    @order_item.update_attributes(order_item_params)
    @order_items = @order.order_items
    redirect_to :back
  end

  def destroy
    @order = current_order
    @order_item = @order.order_items.find(params[:id])
    @order_item.destroy
    @order_items = @order.order_items
    redirect_to :back
  end

  private
    def order_item_params
      params.require(:order_item).permit(:quantity, :product_id)
    end
end
