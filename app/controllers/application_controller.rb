class ApplicationController < ActionController::Base
  # Prevent CSRF attacks by raising an exception.
  # For APIs, you may want to use :null_session instead.
  protect_from_forgery with: :exception
  include SessionsHelper
  
  @title = "F¡zzBuzz!"
  
  # Get the current order
  helper_method :current_order
  def current_order
    #session[:order_id] = nil
    if !session[:order_id].nil?
      begin
        Order.find(session[:order_id])
      rescue ActiveRecord::RecordNotFound
        session[:order_id] = nil
        Order.new
      end
      
    else
      Order.new
    end
  end
  
  helper_method :has_cart
  def has_cart
    current_order.order_items.size != 0
  end
end
