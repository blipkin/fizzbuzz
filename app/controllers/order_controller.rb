class OrderController < ApplicationController
  def create
  end
  
  def show
    #Only allow logged in users to checkout
    redirect_to login_url if !logged_in?
    @order = current_order
    @items = current_order.order_items
    @user = current_user
  end
  
  # THIS IS WHERE ORDERS ARE FAKE COMPLETED
  # BY HAVING THE STATUS CHANGEDAND SESSION CLEARED
  def update
    order = current_order
    order.update_attributes( order_status: 2 )
    order.save
    @order = order
    session[:order_id] = nil
    
    # TRIGGER PUSHER 
    @message = params[:message]
    Pusher['updates_channel'].trigger('app_update', { 
      order: order, 
      products: order.order_items, 
      user: current_user 
    })
    
  end

  # Empty the cart by destroying the order
  def destroy
    order = current_order
    
    # Need to destroy dependant order items first
    OrderItem.where(order_id: order.id).destroy_all
    
    # Optionally destroy order and start new session
    # to prevent accumulation of empty orders
    order.destroy
    
    # Start new session
    session[:order_id] = nil
    
    redirect_to store_url
  end
end
