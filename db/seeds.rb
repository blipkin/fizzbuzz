# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


User.delete_all

User.create!(
    first:  "Ben",
    last: "Lipkin",
    email: "benlipkin@gmail.com",
    city: "New York City",
    region: "New York",
    country: "United States",
    password:              "asdfasdf",
    password_confirmation: "asdfasdf",
    admin: true
)

#secondary admin user
User.create!(
    first:  "Admin",
    last: "User",
    email: "admin@fizzbuzz.com",
    city: "New York City",
    region: "New York",
    country: "United States",
    password:              "password",
    password_confirmation: "password",
    admin: true
)

20.times do |i|
    
  User.create!(
    first:  Faker::Name.first_name,
    last: Faker::Name.last_name,
    email: Faker::Internet.safe_email,
    password: "password",
    password_confirmation: "password",
    city: Faker::Address.city,
    region: Faker::Address.state,
    country: Faker::Address.country
  )
  
end

# Product.delete_all
# 24.times do |n|
#     name = n%2 ==0 ? "Fizz" : "Buzz"
#     Product.create! title: "#{name} Beverage #{n+1}", description: "Tastes uniquly #{name}y", price: 2, active: true
# end

(1..100).each do |i|
    y = i < 10 ? 190 : 19
    year = " (#{y}#{i})"
    name = "Beverage "
    if i % 5 == 0 and i % 3 == 0
        name = "F¡zzBuzz!" + year
    elsif i % 5 == 0
        name = "Buzz! Bracer" 
    elsif i % 3 == 0
        name = "F¡zz Nip"
    end
    Product.create! title: "#{name}", description: "Year: #{year}", price: rand(18...80), active: true
end

