class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first
      t.string :last
      t.email :string
      t.string :password_digest
      t.string :remember_digest
      t.boolean :admin
      t.string :country
      t.string :region
      t.string :city
      t.integer :zip
      t.text :address

      t.timestamps null: false
    end
  end
end
