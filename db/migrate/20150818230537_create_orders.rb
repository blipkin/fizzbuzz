class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.string :order_status
      t.decimal :subtotal
      t.decimal :tax
      t.decimal :shipping
      t.decimal :total

      t.timestamps null: false
    end
  end
end
