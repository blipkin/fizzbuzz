require 'test_helper'

class UsersEditTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:test)
  end

  test "unsuccessful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
    assert_template 'users/edit'
    patch user_path(@user), user: { 
      first:  "",
      email: "nope@invalid",
      password: "short",
      password_confirmation: "diff"
    }
    assert_template 'users/edit'
  end

  
  test "successful edit" do
    log_in_as(@user)
    get edit_user_path(@user)
  end

end