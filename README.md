# README #

* Varsity Tutors sample app: F¡zzBuzz!


### SET UP ###

* ruby 2.2 and rails 4.2
* Clone the repository
* bundle install
* rake db:reset
* Run the automated shopping background job by running rake rake fizzbuzz:roboshop.  Orders will roll in on 5 second intervals, and can be viewed in a live stream on the stats page
* Completing orders requires a user account - Feel free to create your own, or use u: admin@fizzbuzz.com, p: password
* Enjoy!